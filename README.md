# About This Application

handin is an assignment handin manager that assumes your courses are managed in Trello.

# Setup

0. Backup the handin data directory on the remote server from the previous term.
  While it is true that handin should not overwrite old data, you probably should create an archive.
   
1. Edit init-db.yaml for the new semester.

2. python hard-reset-db.py 
  This wipes the tables. Never do this on a running instance; your semester gets worse, right then.

3. python reset-missing-db.py 
  This recreates tables.

4. python init-db.py 
  This walks the course config, and sets things up. You only need to do this if you edit init-db.yaml.
  You should never edit that YAML file once the term has started.
  
5. python run.py
  You should, at this point, have a running local instance.
  
# Creating Controllers

There is a README buried deeper down. Read it. Also, 

python tools/cp.py

checks the syntax of config/controllers.yaml, and 

python tools/cp.py --generate

will generate the skeleton for the application (or, safely extend for existing applications).