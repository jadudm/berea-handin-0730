import sys, os
INTERP = os.path.join(os.environ['HOME'], 'handin.berea.us', 'bin', 'python')
if sys.executable != INTERP:
    os.execl(INTERP, INTERP, *sys.argv)
sys.path.append(os.getcwd())


sys.path.append('application')
from application import app as application
from werkzeug.debug import DebuggedApplication
application = DebuggedApplication(application, True, pin_security = False)
