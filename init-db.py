from peewee import *
from application.models import classes, model
from application.config import *

init = Configuration.from_file('config/init-db.yaml').configure()

def auto ():
  # This gives us the names of the models
  for name in model.__dict__.keys():
    # If we have a config spec for it
    if name in init:
      # Retrieve the model class
      c = model[name]
      # Iterate
      for d in init[name]:
        # Pass the dictionary to the model
        print ("Loading {0}:\n\t{1}".format(name, d))

        # statement = c.insert(**d)
        # sql = statement.sql()
        # print ("\t" + str(sql))
        # print
        # obj = statement.execute()

        o = c(**d)
        # Save it
        o.save(force_insert = True)

if __name__ == "__main__":
  auto ()
