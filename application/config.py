# See the configure documentation for more about
# this library.
# http://configure.readthedocs.io/en/latest/#
from configure import Configuration

# The configure library provides a pretty interface to our
# configuration data. This module doesn't do anything other
# than
config = Configuration.from_file('config/config.yaml').configure()

import os
secret = None
if os.path.exists("config/server_secret.yaml"):
  secret = Configuration.from_file('config/server_secret.yaml').configure()
elif os.path.exists("config/local_secret.yaml"):
  secret = Configuration.from_file('config/local_secret.yaml').configure()


# Load the controllers (if needed)
# controllers = Configuration.from_file('controllers.yaml').configure()
# config.controllers = controllers

# This adds the application's base directory to the
# configuration object, so that the rest of the application
# can reference it.
config.sys.base_dir = os.path.abspath(os.path.dirname(__file__))
