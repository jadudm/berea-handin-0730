from application import app, getMailer
from flask_mail import Message

# Use individual models as:
# s = models.Student()
# assuming Student is a class in studentModel.py
from application.models import model

from application.config import *
from application.logic.validation import tokenOK, isFaculty, isAdmin, isTA
from application.logic.util import getEmailFromToken
from urlparse import urlparse

import datetime
import os
import random
import re
import time
import uuid

from flask import \
  flash, \
  make_response, \
  redirect, \
  render_template, \
  request, \
  session, \
  url_for

THECOOKIE = 'handinbereaus'

# PURPOSE: The initial login page/handler.
@app.route('/', methods = ['GET'])
def email ():
  session['gurumeditation'] = re.sub("-", "", str(str(uuid.uuid4())[:8])).upper()
  session['token'] = None
  session['email'] = None
  resp = make_response(render_template("views/login/emailView.html", config = config))
  return resp
  

# PURPOSE: Handle the token request.
@app.route('/gt', methods = ['POST'])
def handleTokenRequest ():
  T = model.Token
  # Grab their email address and inject it into the token DB.
  emailAddr = request.form['email']

  # Need at least one character in the email address...
  # FIXME: Hopefully this regexp is safer.
  # Using .+ meant that any string was accepted,
  # and ultimately used in a query...
  #if re.match("[a-zA-Z_0-9]+@berea.edu$", emailAddr):
  
  # If their email address is in the Permissions table, we should 
  # go ahead and send them a token.
  P = model.Permission
  q = P.select().where(P.email == emailAddr)
  # print q
  # print q.exists()
  
  if q.exists():
    # Remove old tokens from this user.
    # FIXME: This could be bad... someone guessing can
    # invalidate tokens... we need to make sure
    # existing tokens have lived for some finite period
    # of time...
    q = T.delete().where(T.email == emailAddr)
    q.execute()

    # Now, create the new token.
    now = time.time()
    token = uuid.uuid4()
    # Stick the token in the session
    # NEVER STICK THE TOKEN IN HERE!
    # That's a security issue... stick it in to the handlers that are 
    # exposed in email.
    session['email'] = emailAddr
    # And, in the DB
    # FIXME: Someone guessing addresses can
    # theoretically slam the DB, filling it with
    # bogus requests...
    t = T (email = emailAddr, timestamp = now, token = token)
    t.save()
    o = urlparse(request.url)
    target = "http://"
    target += o.hostname
    if o.port:
      target += ":{0}".format(o.port)
    target += "/s/{0}".format(token)

    upload          = "{0}/u/{1}/upload".format(config.application.url, token)
    download        = "{0}/u/{1}/uploads".format(config.application.url, token)
    course          = "{0}/d/{1}/course".format(config.application.url, token)
    resourceup      = "{0}/r/{1}/choose".format(config.application.url, token)
    resourcedown    = "{0}/r/{1}/download".format(config.application.url, token)
    downloadcourse  = "{0}/d/{1}/course".format(config.application.url, token)
    admin           = "{0}/a/{1}".format(config.application.url, token)

    urls = {  "upload" : upload,
              "download" : download,
              "course": course,
              "resourceup": resourceup,
              "resourcedown": resourcedown,
              "downloadcourse": downloadcourse,
              "admin": admin
              }

    # Only send email if we're not running locally
    if not os.getenv("HANDINLOCAL"):
      msg = Message("Submission @ {0}".format(datetime.datetime.now().strftime("%Y %m %d - %H:%M")),
        sender = "submissionbot@handin.berea.us",
        recipients = [emailAddr]
        )

      msg.body = ""
      if isAdmin(token) or isFaculty(token): # emailAddr in config.permissions.faculty:
        msg.html = render_template("views/email/admin.html",
          config = config,
          urls   = urls,
          random_signature = random.choice(config.random_signatures)
          )
      elif isTA(token):
        msg.html = render_template("views/email/tas.html",
          config = config,
          urls   = urls,
          random_signature = random.choice(config.random_signatures)
          )
      else:
        msg.html = render_template("views/email/submission.html",
          config = config,
          urls   = urls,
          random_signature = random.choice(config.random_signatures)
          )

      with app.app_context():
        mail = getMailer()
        mail.send(msg)

    print ("isFaculty: {0}".format(isFaculty(token)))

    flags = {}
    flags["local"] = os.getenv("HANDINLOCAL")
    flags["faculty"] = isFaculty(token)
    flags["admin"] = isAdmin(token)
    flags["TA"] = isTA(token)

    resp = make_response(
      render_template("views/login/handleTokenRequestView.html",
        config = config,
        urls = urls,
        flags = flags,
        session = session,
        email = emailAddr
        ))
    return resp
  else:
    flash ("It looks like you might need to try again.")
    return redirect(url_for("email"))

@app.route('/a/<string:tok>', methods = ['GET'])
def admin_login (tok):
  if tokenOK(tok):
    if isAdmin(tok) or isTA(tok):
      session['token'] = tok
      return redirect("/admin")
    else:
      flash ("You are not an admin. Darth Vader is looking for you.")
      return redirect(url_for("email"))
  else:
    flash ("You are not an admin. Beware the Jabberwock.")
    return redirect(url_for("email"))

# PURPOSE: Log in with a valid token.
@app.route('/s/<string:tok>', methods = ['GET'])
def login(tok):
  if tokenOK(tok):
    return render_template("views/login/courseSelectionView.html", config = config)
