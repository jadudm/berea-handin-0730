from application import app

# Use individual models as:
# s = models.Student()
# assuming Student is a class in studentModel.py
from application.models import model

from application.config import *
from application.logic.validation import tokenOK, isFaculty, isAdmin, isTA
from application.logic.util import *

from flask import \
  flash, \
  redirect, \
  render_template, \
  request, \
  session, \
  url_for

import datetime, time, re, uuid

class Obj:
  pass

# STARTPOINT
# This is a possible point of external entry from the email.
# session["token"] = tok

# PURPOSE: Select course to upload assignments.
@app.route('/u/<string:tok>/upload', methods = ['GET'])
def uploadcourse (tok):
  if tokenOK(tok):
    T = model.Token
    C = model.Course
    A = model.Assignment
    P = model.Permission

    user = T.select().where(T.token == tok).get()
    courses = []

    session["token"] = tok

    config[tok] = Obj()
    config[tok].user = user
    config[tok].token = tok

    # Make sure admins/TAs are enrolled in classes.
    # That is how we decide who can submit files.
    print("USER: " + user.email)
    print("TOKEN: " + tok)
    q = (P.select().where(P.email == user.email))
    if q.exists():
      courses = map (lambda e: e.course, q)
    else:
      courses = []

    assignments = (A.select(A, C).join(C).where(C.abbreviation == A.course))

    print assignments
    print ("ASSIGNS: {0}".format (map(lambda a: a.name, assignments)))

    config[tok].courses = courses
    config[tok].assignments = assignments

    session['token'] = tok

    return render_template("views/course/uploadcourseView.html",
      config = config,
      token = tok,
      session = session
      )
  else:
    print "RENDERING LOGIN"
    return redirect(url_for('email'))


# PURPOSE: Upload a file to a given assignment
@app.route('/u/<string:tok>/upload/assignid/<int:assignid>', methods = ['POST'])
def uploadForAssignment(tok, assignid):
  if tokenOK(tok):
    A = model.Assignment
    U = model.Upload
    print ("form: {0}".format(request.form))
    print ("data: {0}".format(request.data))

    files  = request.files.getlist("files[]")
    assign = request.form['assignment']
    session['gurumeditation'] = re.sub("-", "", str(str(uuid.uuid4())[:8])).upper()

    print ("Uploading for token {0} to assignment id {1}".format(tok, assignid))
    print ("Assign: " + assign)
    print ("Files: " + str(len(files)))

    filecounter = 0
    
    for f in files:
      extension = getExtension(f.filename).lower()
      
      if (f.filename
          and extension in config.filetypes):

        print ("Trying to save - " + f.filename)

        email = getEmailFromToken(tok)
        courseabbrev = A.get(A.uid == assignid).course.abbreviation
        print "COURSE ABBREV: " + courseabbrev
        
        fileuuid = "{0}-{1}-A{2}-{3}-{4}.{5}".format (
          email,
          courseabbrev,
          assignid,
          time.strftime("%Y%m%d_%H%M%S"),
          filecounter,
          extension
        )
        # fileuuid = email + "-" + courseabbrev + "-A" + str(uuid.uuid4()) + "." + extension
        filedir  = getAssignmentPath() + "/" + courseabbrev
        filepath = filedir + "/" + fileuuid
        
        if not os.path.exists(filedir):
          os.mkdir(filedir)
          
        f.save(filepath)
        filename = filepath
        print ("assignid: " + str(assignid))
        print ("filename: " + filename)
        print ("email:" + getEmailFromToken(tok))

        email = getEmailFromToken(tok)
        ass = U( filename    = f.filename,
                 filepath    = fileuuid,
                 email       = email,
                 timestamp   = datetime.datetime.now(),
                 assignment  = assignid )
        ass.save()
        
        filecounter += 1
      else:
        flash("Could not, would not upload {0}.".format(f.filename))
        return redirect (url_for ("viewUploads", tok = tok))
    return redirect (url_for ("viewUploads", tok = tok))
  else:
    flash("Your token has expired.")
    return redirect (url_for ("email"))

# STARTPOINT
# This is a possible point of external entry from the email.
# session["token"] = tok

@app.route('/u/<string:tok>/uploads', methods = ['GET'])
def viewUploads (tok):
  if tokenOK(tok):
    A = model.Assignment
    U = model.Upload
    email = getEmailFromToken(tok)

    session["token"] = tok

    config[tok] = Obj()
    config[tok].token = tok
    config[tok].uploads = U.select().where(U.email == email).order_by(-U.timestamp)
    config[tok].A = A
    config[tok].U = U
    config[tok].strptime = datetime.datetime.strptime

    return render_template("views/course/uploadForAssignmentView.html",
      config = config,
      token = tok,
      )
  else:
    return redirect (url_for ("email"))

# Download only things for a single user
@app.route('/u/<string:tok>/uploads/<string:student>', methods = ['GET'])
def viewUploadsForStudent (tok, student):
  if tokenOK(tok):
    email = getEmailFromToken(tok)
    if ((student == email) or
        isAdmin(tok) or
        isFaculty(tok) or
        isTA(tok)):
        
      A = model.Assignment
      U = model.Upload

      session["token"] = tok

      config[tok] = Obj()
      config[tok].token = tok
      config[tok].uploads = U.select().where(U.email == student).order_by(-U.timestamp)
      config[tok].A = A
      config[tok].U = U
      config[tok].strptime = datetime.datetime.strptime

      return render_template("views/course/uploadForAssignmentView.html",
        config = config,
        token = tok,
        )
    else:
      return redirect (url_for ("email"))

