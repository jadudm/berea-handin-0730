from application import app

# Use individual models as:
# s = models.Student()
# assuming Student is a class in studentModel.py
from application.models import model

from application.config import *
from application.logic.validation import tokenOK, isAdmin
from application.logic.util import getEmailFromToken

from flask import \
  jsonify, \
  render_template, \
  request, \
  url_for

# PURPOSE: List courses
@app.route('/e/<string:tok>/listcourses', methods = ['GET'])
def listCourses(tok):
  if tokenOK(tok):
    email = getEmailFromToken(tok)
    P = model.Permission
    query = P.select().where(P.email == email)
    return jsonify(map (lambda c: c.name, query))
  else:
    return jsonify({"result": "NO"})


# PURPOSE: Enroll student in course
@app.route('/e/<string:tok>/enroll/<string:email>/<string:abbreviation>', methods = ['GET'])
def enrollStudent(tok, email, abbreviation):
  if isAdmin(tok):
    P = model.Permission
    q = (P.select()
          .where(P.email == email)
          .where(P.course == abbreviation)
          .where(P.role == "STU"))
    if not q.exists():
      p = P(email = email, course = abbreviation, role = "STU")
      p.save()
    return jsonify({"result": "OK"})
  else:
    return jsonify({"result": "NO"})


# PURPOSE: Unenroll student from course
@app.route('/e/<string:tok>/unenroll/<string:email>/<string:abbreviation>', methods = ['GET'])
def unenrollStudent(tok,email,abbreviation):
  if isAdmin(tok):

    return jsonify({"result": "OK"})
  else:
    return jsonify({"result": "NO"})
