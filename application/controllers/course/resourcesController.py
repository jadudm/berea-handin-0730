from application import app

# Use individual models as:
# s = models.Student()
# assuming Student is a class in studentModel.py
from application.models import model
from application.config import *
from application.logic.util import getEmailFromToken, getExtension, getResourcePath
from application.logic.validation import tokenOK, isFaculty, isTA, isAdmin

import re, uuid, os, datetime

from flask import \
    render_template, \
    request, \
    flash, \
    redirect, \
    session, \
    url_for, \
    send_from_directory

class Obj:
  pass

# STARTPOINT
# This is a possible point of external entry from the email.
# session["token"] = tok

# PURPOSE: Choose Resource
@app.route('/r/<string:tok>/choose', methods = ['GET'])
def chooseResource(tok):
  session["token"] = tok
  
  if tokenOK(tok):
    C = model.Course
    P = model.Permission
    config[tok] = Obj()
    q = P.select(P.course).where(P.email == getEmailFromToken(tok) and P.role == "FAC")

    courses = []
    if q.exists():
      courses = map (lambda p: p.course, q)
    config[tok].courses = courses
    print courses
    return render_template("views/course/chooseResourceView.html", config = config, token = tok)
  else:
    flash("Your token has expired.")
    return redirect (url_for ("email"))

# http://stackoverflow.com/questions/7406102/create-sane-safe-filename-from-any-unsafe-string
def cleanupFilePath (filename):
  keepcharacters = (' ','.','_','-')
  cleaned = "".join([c for c in filename if c.isalpha() or c.isdigit() or c in keepcharacters]).rstrip()
  return cleaned

# PURPOSE: Add Resource
@app.route('/r/<string:tok>/add/<string:abbreviation>', methods = ['POST'])
def addResource(tok, abbreviation):
  if tokenOK(tok) and (isAdmin(tok) or isFaculty(tok) or isTA(tok)) :
    R = model.Resource


    f      = request.files["file"]
    desc   = request.form["filedesc"]
    assign = request.form["course"]
    session['gurumeditation'] = re.sub("-", "", str(str(uuid.uuid4())[:8])).upper()

    print ("Uploading resource for token {0} to course {1}".format(tok, abbreviation))
    print ("File: {0} Name: {1}".format(f, f.filename))

    if (f.filename):
      print ("Trying to save resource - " + f.filename)
      extension = getExtension(f.filename)
      cleaned   = cleanupFilePath(f.filename)
      coursepath = getResourcePath() + "/" + abbreviation
      if not os.path.exists(coursepath):
        os.mkdir(coursepath)

      filepath = coursepath + "/" + cleaned
      f.save(filepath)

      email = getEmailFromToken(tok)
      q = R.select().where (R.filename == cleaned, R.course == abbreviation)
      if q.exists():
        print ("UPDATING")

        res = (R.update (filename    = cleaned,
                  filepath    = filepath,
                  email       = email,
                  description = desc,
                  timestamp   = datetime.datetime.now(),
                  course      = abbreviation )
                .where(R.filename == cleaned, R.course == abbreviation))
        res.execute()
      else:
        res = R(  filename    = cleaned,
                  filepath    = filepath,
                  description = desc,
                  email       = email,
                  timestamp   = datetime.datetime.now(),
                  course      = abbreviation )
        res.save()
      return redirect (url_for ("chooseResourceToDownload", tok = tok))
    else:
      flash("Could not, would not upload resource {0}.".format(f.filename))
      return redirect (url_for ("email", tok = tok))

  else:
    flash("Your token has expired.")
    return redirect (url_for ("email"))

  return render_template("views/course/addResourceView.html", config = config)

# STARTPOINT
# This is a possible point of external entry from the email.
# session["token"] = tok

@app.route('/r/<string:tok>/download', methods = ['GET'])
def chooseResourceToDownload (tok):
  session["token"] = tok
  
  if tokenOK(tok):
    R = model.Resource
    C = model.Course
    P = model.Permission
    email = getEmailFromToken(tok)

    incourses = P.select().where(P.email == email)
    incourses = map(lambda c: c.course.abbreviation, incourses)
    print "{0}".format(incourses)

    resources = []
    courses = []

    cq = R.select(R.course).distinct()

    resources = {}

    if cq.exists():
      for c in cq:
        if (c.course.abbreviation in incourses
          or "ANY" in incourses):
          courses.append(c)
          resources[c.course.abbreviation] = []

          q = (R.select()
            .where(R.course == c.course.abbreviation)
            .order_by(R.timestamp.desc())
            )

          if q.exists():
            for r in q:
              resources[str(c.course.abbreviation)].append(r)

    return render_template("views/course/chooseDownloadView.html",
      config = config,
      courses = courses,
      token = tok,
      resources = resources)
  else:
    flash("Your token has expired.")
    return redirect (url_for ("email"))

# PURPOSE: Download Resources
@app.route('/r/<string:tok>/download/<int:rid>', methods = ['GET'])
def downloadResource(tok, rid):
  if tokenOK(tok):
    R = model.Resource
    q = R.get(R.uid == rid)
    filename = q.filename
    print filename
    path = getResourcePath() + "/" + q.course.abbreviation
    print path
    if os.path.exists(path + "/" + filename):
      print "Found full path; sending."
      return send_from_directory(path, filename, as_attachment = True,
        mimetype='application/octet-stream')
    else:
      return redirect(url_for("email"))
  else:
    return redirect(url_for("email"))

# PURPOSE: Remove Resources
@app.route('/r/<string:tok>/remove/<int:rid>', methods = ['GET'])
def removeResource(tok,rid):
  return render_template("views/course/removeResourceView.html", config = config)
