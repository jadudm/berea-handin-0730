from application import app
from peewee import fn

# Use individual models as:
# s = models.Student()
# assuming Student is a class in studentModel.py
from application.models import model

from application.config import *
from application.logic.validation import tokenOK, isStudent, isAdmin, isFaculty, isTA
from application.logic.util import \
  zipdir, \
  getEmailFromToken, \
  getAssignmentZipPath, \
  getAssignmentPath

from flask import \
  flash, \
  session, \
  redirect, \
  render_template, \
  request, \
  send_from_directory, \
  url_for

import datetime, os, re, shutil, zipfile

# STARTPOINT
# This is a possible point of external entry from the email.
# session["token"] = tok

# PURPOSE: Select course to download files.
@app.route('/d/<string:tok>/course', methods = ['GET'])
def downloadcourse(tok):
  P = model.Permission

  session["token"] = tok

  if (tokenOK(tok)):
    if isStudent(tok):
      flash("You are not an administrator. Daleks are en route to your location.")
      return redirect(url_for("email"))
  else:
    flash ("Token expired or invalid.")
    return redirect(url_for("email"))

  email = getEmailFromToken(tok)

  if isAdmin(tok):
    courses = P.select().where(P.role != "ADMIN")
  if isFaculty(tok):
    courses = P.select().where(P.role == "FAC" and P.email == email)
  if isTA(tok):
    courses = P.select().where(P.role == "TA" and P.email == email)

  uniq = []
  for c in courses:
    if c.course.abbreviation not in map (lambda p: p.course.abbreviation, uniq):
      uniq.append(c)

  session["token"] = tok
  return render_template("views/course/downloadcourseView.html",
    config  = config,
    session = session,
    model   = model,
    courses = uniq,
    fn = fn
    )

# PURPOSE: Download a file.
@app.route('/d/<string:tok>/download/<string:course>/<string:filename>', methods = ['GET'])
def downloadfile(tok,course,filename):
  if tokenOK(tok):
    return send_from_directory(getAssignmentPath() + "/" + course, filename, as_attachment = True,
      mimetype='application/octet-stream')
  else:
    return redirect(url_for("email"))

@app.route('/d/<string:tok>/download/zip/<string:zipfile>')
def downloadZip(tok, zipfile):
  if tokenOK(tok):
    if (isFaculty(tok) or isAdmin(tok) or isTA(tok)):
      return send_from_directory(getAssignmentZipPath(), zipfile)
  else:
    flash ("Token expired or invalid.")
    return redirect(url_for("email"))

# PURPOSE: Generate downloadable zip for an assignment
@app.route('/d/<string:tok>/download/assign/<string:tag>/<int:assignid>', methods = ['GET'])
def generateAssignmentZip(tok,tag,assignid):
  print "1"
  if tokenOK(tok):
    U = model.Upload
    A = model.Assignment
    P = model.Permission

    TAOK = False
    if isTA(tok):
      print "2"
      email = getEmailFromToken(tok)
      courses = P.select().where(P.email == email)
      a = A.get(A.uid == assignid)
      if a.course.abbreviation in map(lambda p: p.course.abbreviation, courses):
        TAOK = True
    
    if (isFaculty(tok) or isAdmin(tok) or TAOK):
      print "3"
      if tag == "norepeat":
        uploads = (U.select()
                    .where(U.assignment == assignid)
                    .group_by(U.email)
                    .having(U.timestamp == fn.MAX(U.timestamp))
                    )
                    
      else: # tag == "all" or anything at this point
        uploads = U.select().where(U.assignment == assignid)
      theA = A.select().where(A.uid == assignid).get()

      if not os.path.exists(getAssignmentZipPath()):
        os.mkdir(getAssignmentZipPath())

      # FIXME: There's a method for this. I did this the wrong way
      # somewhere else, too.
      ziproot = datetime.datetime.now().strftime("%Y%m%d-%H%M%S-{0}-{1}".format(theA.course.abbreviation, re.sub(" ", "", theA.name)))
      tgtdir = (getAssignmentZipPath() + "/" + ziproot)

      print "3"
      # Remove the dir if it exists.
      if os.path.exists(tgtdir):
        shutil.rmtree(tgtdir)
      os.mkdir(tgtdir)

      for u in uploads:
        print "4"
        filename, file_extension = os.path.splitext(u.filepath)
        targetname = "{0}-{3}-A{4}-{1}{2}".format(
          u.email,
          #re.sub("@[a-zA-Z]+\.(edu|com|org|net)", "", u.email), 
          u.timestamp.strftime("%Y%m%d-%H%M%S"), 
          file_extension, 
          theA.course.abbreviation,
          theA.uid)
        shutil.copyfile(getAssignmentPath() + "/" + theA.course.abbreviation + "/" + u.filepath, tgtdir + "/" + targetname)

      print "5"
      zipfilename = '{0}.zip'.format(tgtdir)
      zipf = zipfile.ZipFile(zipfilename, 'w', zipfile.ZIP_DEFLATED)
      zipdir(tgtdir, zipf)
      zipf.close()
      print "6"
      return redirect(url_for("downloadZip", tok = tok, zipfile = ziproot + ".zip"))

    else: # not faculty or admin
      flash("You are not an administrator. The Cybermen are on their way.")
      return redirect(url_for("email"))
  else: # not OK
    flash ("Token expired or invalid.")
    return redirect(url_for("email"))