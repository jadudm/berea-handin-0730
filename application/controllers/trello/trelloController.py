from application import app
from trello import TrelloApi, Boards, Actions, Lists
import datetime
import dateutil.parser
import pytz

# Use individual models as:
# s = models.Student()
# assuming Student is a class in studentModel.py
from application.models import model

from application.config import *

from flask import \
    render_template, \
    request, \
    url_for

from application.config import secret

# For interacting with Trello
KEY = secret.trello_key
t = TrelloApi(KEY)
b = Boards(KEY)
l = Lists(KEY)


def hashandin (c):
  hashandin = False
  for lab in c['labels']:
    # Once true, it stays true
    # There must be a due date attached for us to consider 
    # this something that needs to be handed in. Do, if it is labeled
    # 'handin' but no date is attached, we do not claim it is a 'handin' card.
    hashandin = ((lab['name'] == "handin") or hashandin) and (c['due'] != None)
  return hashandin
      

# PURPOSE: Lists assignments from the course.
@app.route('/t/<string:courseid>', methods = ['GET'])
def listAssignments(courseid):
  lists = b.get_list("x4m5GIFw")
  meta  = b.get("x4m5GIFw")
  assigns = []
  for ls in lists:
    if 'id' in ls:
      print ("List ID: " + ls['id'] + " " + ls['name'])
      for c in l.get_card(ls['id']):
        print c['id'] + " " + c['name']
        if hashandin(c):
          parsed =  dateutil.parser.parse(c['due']) + datetime.timedelta(hours=config.application.timezone)
          print "\tdue: {0}".format(parsed)
          assign = {}
          assign['id'] = c['id']
          assign['name'] = c['name']
          assign['due'] = parsed
          assigns.append(assign)
          
  print ("ASSIGNS: ")
  print (assigns)
  # Sort the assignments by date.
  assigns.sort(key = lambda x: x['due'], reverse = True)
  now = datetime.datetime.now().replace(tzinfo=pytz.UTC)
  upcoming = [ a for a in assigns if a['due'] >= now ]
  past     = [ a for a in assigns if a['due'] < now ]
  
  print past
  
  return render_template("views/trello/listAssignmentsView.html", 
                          config = config, 
                          assignments = assigns,
                          meta = meta,
                          upcoming = upcoming,
                          past = past
                          )

