from application.models.util import *
import datetime

class Course (Model):
  abbreviation    = TextField(primary_key = True)
  name            = TextField()
  term            = TextField()
  # This is the Trello board ID. Board must be public.
  trello          = TextField()

  def __str__(self):
    return self.abbreviation

  class Meta:
    database = getDB("course")
