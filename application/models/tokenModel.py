from application.models.util import *

class Token (Model):
  tid           = PrimaryKeyField()
  email         = TextField()
  token         = TextField()
  timestamp     = TextField()

  class Meta:
    database = getDB("tokens")
