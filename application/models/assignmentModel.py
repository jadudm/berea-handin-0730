from application.models.util import *
from application.models.courseModel import Course
import datetime

class Assignment (Model):
  uid             = PrimaryKeyField()
  name            = TextField()
  course          = ForeignKeyField(Course)
  due             = DateTimeField()

  def __str__(self):
    return self.name

  class Meta:
    database = getDB("course")
