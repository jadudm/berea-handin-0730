from application.models.util import *
from application.models.assignmentModel import Assignment
import datetime

class Upload (Model):
  uid             = PrimaryKeyField()
  filename        = TextField()
  filepath        = TextField()
  email           = TextField()
  timestamp       = DateTimeField(default = datetime.datetime.now)
  assignment      = ForeignKeyField (Assignment)

  class Meta:
    database = getDB("course")
