from application.models.util import *
from application.models.courseModel import Course
from application.models.roleTypeModel import RoleType
import datetime

class Permission (Model):
  uid             = PrimaryKeyField()
  email           = TextField()
  course          = ForeignKeyField(Course)
  role            = ForeignKeyField(RoleType)

  def __str__(self):
    return self.abbreviation

  class Meta:
    database = getDB("permissions")
