from application.models.util import *

class RoleType (Model):
  role          = TextField(primary_key = True)

  def __str__(self):
    return self.role

  class Meta:
    database = getDB("permissions")
