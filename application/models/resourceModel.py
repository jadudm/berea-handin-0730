from application.models.util import *
from application.models.courseModel import Course
import datetime

class Resource (Model):
  uid             = PrimaryKeyField()
  filename        = TextField()
  filepath        = TextField()
  description     = TextField()
  email           = TextField()
  timestamp       = DateTimeField(default = datetime.datetime.now)
  course          = ForeignKeyField (Course)

  class Meta:
    database = getDB("resource")
