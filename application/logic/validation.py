# See the configure documentation for more about
# this library.
# http://configure.readthedocs.io/en/latest/#
from configure import Configuration
from application.config import config
from application.models import getModelFromName
from functools import wraps
from flask import request, redirect, url_for
import os, re, time
from application.models import model

# roleConfig = Configuration.from_file('config/roles.yaml').configure()

def checkRoleFromToken (tok, role):
  T  = model.Token
  P  = model.Permission
  RT = model.RoleType

  # If I was better, I'd be able to do this in one query.
  q1 = (T.select()
        .where(T.token == tok))

  if q1.exists():
    email = q1.get().email
    q2 = (P.select()
          .join(RT)
          .where(P.email == email)
          .where(RT.role == role)
          )
    if q2.exists():
      return True
  return False

def isAdmin (tok):
  return tokenOK(tok) and checkRoleFromToken(tok, "ADMIN")

def isFaculty(tok):
  return tokenOK(tok) and checkRoleFromToken(tok, "FAC")

def isTA (tok):
  return tokenOK(tok) and checkRoleFromToken(tok, "TA")

def isStudent (tok):
  return tokenOK(tok) and checkRoleFromToken(tok, "STU")

def tokenOK (tok):
  T = model.Token
  q = model.Token.select().where(T.token == tok)
  if q.exists():
    print "Token Found: {0}".format (tok)
    user = T.get(T.token == tok)
    now = time.time()
    
    isTokenValid  = False
    timeLeft      = (now - float(user.timestamp))
    
    if checkRoleFromToken(tok, "ADMIN"):
      # Give admins 5x more time.
      isTokenValid = timeLeft < (config.application.tokenTimeout * 60 * 5)
    else:
      isTokenValid = timeLeft < (config.application.tokenTimeout * 60)
      
    if isTokenValid:
      print "We have {0} left.".format(timeLeft)
      return True
  return False
