from application.models import model
import re
import zipfile
import os
from application.config import *

def zipdir(path, ziph):
  # ziph is zipfile handle
  for root, dirs, files in os.walk(path):
    for file in files:
      f = os.path.join(root, file)
      print (f)
      ziph.write(f , re.sub(getAssignmentZipPath(), "", f))

def getEmailFromToken (tok):
  T = model.Token
  q = T.select().where(T.token == tok)
  if q.exists():
    return T.select().where(T.token == tok).get().email
  else:
    return False

def getExtension (fname):
  m = re.match(".*\.(.*?)$", fname)
  if m:
    return m.group(1)
  else:
    return "unknown"

def getAssignmentPath ():
  if os.getenv("HANDINLOCAL"):
    assignmentsPath = config.uploads.dev.path
    zipPath         = config.uploads.dev.zipdir
  else:
    assignmentsPath = config.uploads.prod.path
    zipPath         = config.uploads.prod.zipdir
  return assignmentsPath

def getAssignmentZipPath ():
  if os.getenv("HANDINLOCAL"):
    assignmentsPath = config.uploads.dev.path
    zipPath         = config.uploads.dev.zipdir
  else:
    assignmentsPath = config.uploads.prod.path
    zipPath         = config.uploads.prod.zipdir
  return zipPath

def getResourcePath ():
  if os.getenv("HANDINLOCAL"):
    assignmentsPath = config.uploads.dev.path
    zipPath         = config.uploads.dev.zipdir
    resPath         = config.uploads.dev.res
  else:
    assignmentsPath = config.uploads.prod.path
    zipPath         = config.uploads.prod.zipdir
    resPath         = config.uploads.prod.res
  return resPath
