# http://pythonhosted.org/trello/trello.html
from trello import TrelloApi, Boards, Actions, Lists
import datetime
import dateutil.parser

KEY = "aa4be784d2cb32ecce033bf9153853ba"

PROGLANG = 'x4m5GIFw'
CRAFTOE  = 'aheINjUy'

t = TrelloApi(KEY)
b = Boards(KEY)
l = Lists(KEY)

print (t.boards.get(PROGLANG))
print
lists = b.get_list(PROGLANG)
print (l)


def hashandin (c):
  hashandin = False
  for lab in c['labels']:
    # Once true, it stays true
    # There must be a due date attached for us to consider 
    # this something that needs to be handed in. Do, if it is labeled
    # 'handin' but no date is attached, we do not claim it is a 'handin' card.
    hashandin = ((lab['name'] == "handin") or hashandin) and (c['due'] != None)
      
  return hashandin
  
for ls in lists:
  # [{u'idBoard': u'587111bd43082aa3d0425f35', u'subscribed': None, u'pos': 65535.5, u'closed': False, u'id': u'587111c099377a2c6828b1fc', u'name': u'Admin'}, {u'idBoard': u'587111bd43082aa3d0425f35', u'subscribed': None, u'pos': 131071, u'closed': False, u'id': u'587111c320a2a733a5285b39', u'name': u'Assignments'}, {u'idBoard': u'587111bd43082aa3d0425f35', u'subscribed': None, u'pos': 163839, u'closed': False, u'id': u'58762dacb0a2ae80667d2ff4', u'name': u'Things in the Past'}, {u'idBoard': u'587111bd43082aa3d0425f35', u'subscribed': None, u'pos': 196607, u'closed': False, u'id': u'587111cb57b2dcf50588bb83', u'name': u'Language Explorations'}, {u'idBoard': u'587111bd43082aa3d0425f35', u'subscribed': None, u'pos': 327679, u'closed': False, u'id': u'589b2f4b091348b879450ae3', u'name': u'Resources'}]
  if 'id' in ls:
    print ("List ID: " + ls['id'] + " " + ls['name'])
    for c in l.get_card(ls['id']):
      # {u'labels': [{u'color': u'lime', u'uses': 5, u'id': u'58711a0bced82109ffa0ef10', u'idBoard': u'587111bd43082aa3d0425f35', u'name': u'Resource'}], u'pos': 32767.5, u'manualCoverAttachment': False, u'id': u'589b2d091c0c970fccfcae71', u'badges': {u'votes': 0, u'attachments': 1, u'subscribed': False, u'due': u'2017-02-08T17:00:00.000Z', u'comments': 0, u'dueComplete': False, u'checkItemsChecked': 0, u'fogbugz': u'', u'viewingMemberVoted': False, u'checkItems': 0, u'description': True}, u'idBoard': u'587111bd43082aa3d0425f35', u'idShort': 19, u'due': u'2017-02-08T17:00:00.000Z', u'dueComplete': False, u'shortUrl': u'https://trello.com/c/xDzzXPdJ', u'closed': False, u'subscribed': False, u'dateLastActivity': u'2017-02-10T01:40:57.524Z', u'idList': u'589b2f4b091348b879450ae3', u'idMembersVoted': [], u'idLabels': [u'58711a0bced82109ffa0ef10'], u'idMembers': [], u'checkItemStates': None, u'desc': u'The Onion Code from Feb 8th.\n', u'descData': {u'emoji': {}}, u'name': u'Onion Code', u'shortLink': u'xDzzXPdJ', u'idAttachmentCover': None, u'url': u'https://trello.com/c/xDzzXPdJ/19-onion-code', u'idChecklists': []}
      print c['id'] + " " + c['name']
      if hashandin(c):
        parsed =  dateutil.parser.parse(c['due'])
        print "\tdue: {0}".format(parsed)
 