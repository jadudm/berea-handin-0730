venvsetup() {
    if hash conda 2>/dev/null; then
        # https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/20/conda/
        conda create -n venv python=2.7 anaconda
        source activate venv
        # There is no Trello in anaconda
        conda install -n venv trello
    else
        virtualenv venv
        source venv/bin/activate
        pip install --upgrade pip
        pip install trello
        pip install python-dateutil
    fi
}

if [ -d venv ] 
then 
  rm -rf venv
fi

alias python=python2.7
virtualenv venv
source venv/bin/activate
pip install --upgrade pip
pip install trello
pip install python-dateutil
